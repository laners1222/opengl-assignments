#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_fire : public scene
{
public:
	~scene_fire();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void pool_refresh_flama(int pool_rate);
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void calentar_llamas();

private:
	

};