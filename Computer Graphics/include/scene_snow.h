#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_snow : public scene
{
public:
	~scene_snow();

	void init();
	void refrescar_sistema_configurable_nieve();
	void awake();
	void sleep();
	void reset() { }
	void pool_particle_reloader_refresher(int tasa_pool);
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

	

private:
	

};
