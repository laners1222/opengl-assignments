#pragma once

#include "scene.h"
#include <IL/il.h>

class scene_texture : public scene
{
public:


	~scene_texture();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	GLuint shader_program;
	GLuint vao;
	GLuint positionsVBO;
	GLuint colorVBO;
	GLuint normalsVBO;
	GLuint coordsVBO;
	GLuint VERTEX_BUFFER_O_IND;

	ILuint imageID;
	GLuint VERTEX_BUFFER_O_TEXT;
	ILuint imageID2;
	GLuint VERTEX_BUFFER_O_TEXT2;
};