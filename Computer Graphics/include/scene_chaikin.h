#pragma once

#include "scene.h"
#include "vec2.h"
#include <vector>

class scene_chaikin : public scene
{
public:
	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void chaikin(std::vector<cgmath::vec2>& positions);

private:
	int countCuerpo;
	int countBrazoIzq;
	int countBrazoDer;
	int countPieIzq;
	int countPieDer;
	int countODER;
	int countIZQ;
	int countSONRISA;

	GLuint vao;
	GLuint positionVBO;

	GLuint vao2;
	GLuint positionVBO2;

	GLuint vao3;
	GLuint positionVBO3;

	GLuint vao4;
	GLuint positionVBO4;

	GLuint vao5;
	GLuint positionVBO5;

	GLuint vao6;
	GLuint positionVBO6;

	GLuint vao7;
	GLuint positionVBO7;

	GLuint vao8;
	GLuint positionVBO8;

	GLuint vao9;
	GLuint positionVBO9;

	GLuint vao10;
	GLuint positionVBO10;

	GLenum primitiveType;
};