

#include "scene.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_fire : public scene
{
public:
	~scene_fire();

	void cargar_variables_fisicas(int i, float variante_color, float posicion_x, float posicion_y, float width_pos);

	void cargar_GL();

	void generar_poligono();

	void init();
	void configurar_particula(int i, int j);
	void cargar_configuraciones_sistema();
	void change_conditions_for(int i, float desfase_velocidad, float tiempo);
	void actualizar_sistema();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void update();


private:
	GLuint shader_program;
	GLuint vao;
	GLuint sidesVBO;
	GLuint colorsVBO;
	GLuint coordVBO;
	GLuint normalVBO;
	GLuint indicesBuffer;

	GLuint textureId;
	float wtl_ratio_aspc;
	std::vector<cgmath::vec4> system_colors;
	std::vector<cgmath::vec3> system_positions;
	std::vector<cgmath::vec3> system_speeds;
	std::vector<GLfloat> system_lifecycle;
	std::vector<GLfloat> system_revival;

};
