#include "scene_chaikin.h"
#include "vec2.h"
#include <vector>
void scene_chaikin::chaikin(std::vector<cgmath::vec2>& positions) {
	std::vector<cgmath::vec2> temp;
	temp.push_back(positions[0]);
	for (unsigned int i = 0; i < (positions.size() - 1); ++i) {
		const cgmath::vec2& p0 = positions[i];
		const cgmath::vec2& p1 = positions[i + 1];
		cgmath::vec2 Q;
		cgmath::vec2 R;
		Q.x = 0.75f*p0.x + 0.25f*p1.x;
		Q.y = 0.75f*p0.y + 0.25f*p1.y;
		R.x = 0.25f*p0.x + 0.75f*p1.x;
		R.y = 0.25f*p0.y + 0.75f*p1.y;
		temp.push_back(Q);
		temp.push_back(R);
	}
	temp.push_back(positions[positions.size() - 1]);
	positions = temp;
}
void scene_chaikin::init(){
	primitiveType = GL_LINE_LOOP;
	std::vector<cgmath::vec2> mainCurveLoopFORBODY;
	std::vector<cgmath::vec2> oderecho;
	std::vector<cgmath::vec2> oizquierdo;
	std::vector<cgmath::vec2> sonrisa;
	mainCurveLoopFORBODY.push_back(cgmath::vec2(0.0f, 0.5f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.1f, 0.5f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.3f, 0.4f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.4f, 0.3f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.5f, 0.1f));
	mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.5f, -0.1f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.4f, -0.3f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.3f, -0.4f));mainCurveLoopFORBODY.push_back(cgmath::vec2(-0.1f, -0.5f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.1f, -0.5f));
	mainCurveLoopFORBODY.push_back(cgmath::vec2(0.3f, -0.4f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.4f, -0.3f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.5f, -0.1f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.5f, 0.1f));
	mainCurveLoopFORBODY.push_back(cgmath::vec2(0.4f, 0.3f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.3f, 0.4f));mainCurveLoopFORBODY.push_back(cgmath::vec2(0.1f, 0.5f));
	mainCurveLoopFORBODY.push_back(cgmath::vec2(0.0f, 0.5f));
	oderecho.push_back(cgmath::vec2(-0.2f, 0.2f));
	oderecho.push_back(cgmath::vec2(-0.3f, 0.1f));
	oderecho.push_back(cgmath::vec2(-0.2f, 0.0f));
	oderecho.push_back(cgmath::vec2(-0.1f, 0.1f));
	oderecho.push_back(cgmath::vec2(-0.2f, 0.2f));
	oizquierdo.push_back(cgmath::vec2(0.2f, 0.2f));
	oizquierdo.push_back(cgmath::vec2(0.3f, 0.1f));
	oizquierdo.push_back(cgmath::vec2(0.2f, 0.0f));
	oizquierdo.push_back(cgmath::vec2(0.1f, 0.1f));
	oizquierdo.push_back(cgmath::vec2(0.2f, 0.2f));
	sonrisa.push_back(cgmath::vec2(-0.3f, -0.2f));
	sonrisa.push_back(cgmath::vec2(-0.2f, -0.25f));
	sonrisa.push_back(cgmath::vec2(-0.1f, -0.3f));
	sonrisa.push_back(cgmath::vec2(0.0f, -0.35f));
	sonrisa.push_back(cgmath::vec2(0.1f, -0.3f));
	sonrisa.push_back(cgmath::vec2(0.2f, -0.25f));
	sonrisa.push_back(cgmath::vec2(0.3f, -0.2f));
	chaikin(mainCurveLoopFORBODY);
	chaikin(mainCurveLoopFORBODY);
	chaikin(mainCurveLoopFORBODY);
	chaikin(mainCurveLoopFORBODY);
	chaikin(mainCurveLoopFORBODY);
	chaikin(mainCurveLoopFORBODY);
	chaikin(oderecho);
	chaikin(oderecho);
	chaikin(oderecho);
	chaikin(oderecho);
	chaikin(oderecho);
	chaikin(oderecho);
	chaikin(oizquierdo);
	chaikin(oizquierdo);
	chaikin(oizquierdo);
	chaikin(oizquierdo);
	chaikin(oizquierdo);
	chaikin(oizquierdo);
	chaikin(sonrisa);
	chaikin(sonrisa);
	chaikin(sonrisa);
	chaikin(sonrisa);
	chaikin(sonrisa);
	chaikin(sonrisa);
	countCuerpo = mainCurveLoopFORBODY.size();
	countODER = oderecho.size();
	countIZQ = oizquierdo.size();
	countSONRISA = sonrisa.size();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &positionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * mainCurveLoopFORBODY.size(), mainCurveLoopFORBODY.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glGenVertexArrays(1, &vao8);
	glBindVertexArray(vao8);
	glGenBuffers(1, &positionVBO8);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO8);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * sonrisa.size(), sonrisa.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glGenVertexArrays(1, &vao6);
	glBindVertexArray(vao6);
	glGenBuffers(1, &positionVBO6);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO6);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * oderecho.size(), oderecho.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glGenVertexArrays(1, &vao7);
	glBindVertexArray(vao7);
	glGenBuffers(1, &positionVBO7);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO7);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * oizquierdo.size(), oizquierdo.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	std::vector<cgmath::vec2> brazoIzq;
	brazoIzq.push_back(cgmath::vec2(-0.495f, 0.1f));
	brazoIzq.push_back(cgmath::vec2(-0.7f, -0.3f));
	brazoIzq.push_back(cgmath::vec2(-0.6f, -0.4f));
	brazoIzq.push_back(cgmath::vec2(-0.4f, -0.3f));
	chaikin(brazoIzq);
	chaikin(brazoIzq);
	chaikin(brazoIzq);
	countBrazoIzq = brazoIzq.size();
	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);
	glGenBuffers(1, &positionVBO2);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * brazoIzq.size(), brazoIzq.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	std::vector<cgmath::vec2> brazoDer;
	brazoDer.push_back(cgmath::vec2(0.495f, 0.1f));
	brazoDer.push_back(cgmath::vec2(0.7f, -0.3f));
	brazoDer.push_back(cgmath::vec2(0.6f, -0.4f));
	brazoDer.push_back(cgmath::vec2(0.4f, -0.3f));
	chaikin(brazoDer);
	chaikin(brazoDer);
	chaikin(brazoDer);
	countBrazoDer = brazoDer.size();
	glGenVertexArrays(1, &vao3);
	glBindVertexArray(vao3);
	glGenBuffers(1, &positionVBO3);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * brazoDer.size(), brazoDer.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	std::vector<cgmath::vec2> pieIzq;
	pieIzq.push_back(cgmath::vec2(-0.3f, -0.4f));
	pieIzq.push_back(cgmath::vec2(-0.2f, -0.5f));
	pieIzq.push_back(cgmath::vec2(-0.6f, -0.55f));
	pieIzq.push_back(cgmath::vec2(-0.6f, -0.7f));
	pieIzq.push_back(cgmath::vec2(0.0f, -0.7f));
	pieIzq.push_back(cgmath::vec2(-0.15f, -0.5f));
	chaikin(pieIzq);
	chaikin(pieIzq);
	chaikin(pieIzq);
	countPieIzq = pieIzq.size();
	glGenVertexArrays(1, &vao4);
	glBindVertexArray(vao4);
	glGenBuffers(1, &positionVBO4);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * pieIzq.size(), pieIzq.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	std::vector<cgmath::vec2> pieDer;
	pieDer.push_back(cgmath::vec2(0.3f, -0.4f));
	pieDer.push_back(cgmath::vec2(0.2f, -0.5f));
	pieDer.push_back(cgmath::vec2(0.6f, -0.55f));
	pieDer.push_back(cgmath::vec2(0.6f, -0.7f));
	pieDer.push_back(cgmath::vec2(0.0f, -0.7f));
	pieDer.push_back(cgmath::vec2(0.15f, -0.5f));
	chaikin(pieDer);
	chaikin(pieDer);
	chaikin(pieDer);
	countPieDer = pieDer.size();
	glGenVertexArrays(1, &vao5);
	glBindVertexArray(vao5);
	glGenBuffers(1, &positionVBO5);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO5);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * pieDer.size(), pieDer.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
void scene_chaikin::awake(){
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(1.0f);
}

void scene_chaikin::sleep(){
	glPointSize(1.0f);
}

void scene_chaikin::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindVertexArray(vao);
	glDrawArrays(primitiveType, 0, countCuerpo);
	glBindVertexArray(0);
	glBindVertexArray(vao2);
	glDrawArrays(GL_LINE_STRIP, 0, countBrazoIzq);
	glBindVertexArray(0);
	glBindVertexArray(vao3);
	glDrawArrays(GL_LINE_STRIP, 0, countBrazoDer);
	glBindVertexArray(0);
	glBindVertexArray(vao4);
	glDrawArrays(GL_LINE_STRIP, 0, countPieIzq);
	glBindVertexArray(0);
	glBindVertexArray(vao5);
	glDrawArrays(GL_LINE_STRIP, 0, countPieDer);
	glBindVertexArray(0);
	glBindVertexArray(vao6);
	glDrawArrays(GL_LINE_LOOP, 0, countODER);
	glBindVertexArray(0);
	glBindVertexArray(vao7);
	glDrawArrays(GL_LINE_LOOP, 0, countIZQ);
	glBindVertexArray(0);
	glBindVertexArray(vao8);
	glDrawArrays(GL_LINE_STRIP, 0, countSONRISA);
	glBindVertexArray(0);
}

void scene_chaikin::normalKeysDown(unsigned char key){
	if (key == '1') primitiveType = GL_POINTS;
	if (key == '2') primitiveType = GL_LINES;
	if (key == '3') primitiveType = GL_LINE_STRIP;
	if (key == '4') primitiveType = GL_LINE_LOOP;
	if (key == '5') primitiveType = GL_TRIANGLES;
	if (key == '6') primitiveType = GL_TRIANGLE_STRIP;
	if (key == '7') primitiveType = GL_TRIANGLE_FAN;
}

