#include "scene_circle.h"
#include <math.h>
#include "ifile.h"
#include <string>
#include "vec2.h"
#include "vec3.h"
#include <vector>


void scene_circle::init(){
	int number = 40;
	float radius = 1.0f;
	float escala = 2*3.1416f;
	std::vector<cgmath::vec2> circle;
	std::vector<cgmath::vec3> color;
	circle.push_back(cgmath::vec2(0.0f, 0.0f));
	color.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
	for (int i = 0; i <= 40; i++) {
		color.push_back(cgmath::vec3(radius*cosf(i*escala / number), radius*sinf(i*escala / number), (radius*cosf(i*escala / number)) * (radius*sinf(i*escala / number))));
		circle.push_back(cgmath::vec2(radius*cosf(i*escala / number), radius*sinf(i*escala / number)));
	}
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2)*circle.size(), circle.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &colorVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3)*color.size(), color.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	ifile shader_file;
	shader_file.read("shaders/Default.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)
		vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c,
		nullptr);
	glCompileShader(vertex_shader);
	shader_file.read("shaders/discard.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)
		fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c,
		nullptr);
	glCompileShader(fragment_shader);
	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "VertexColor");
	glLinkProgram(shader_program);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400, 400);
	glUseProgram(0);
}

void scene_circle::awake(){
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void scene_circle::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader_program);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 42);
	glBindVertexArray(0);
	glUseProgram(0);
}

void scene_circle::resize(int width, int height){
	glViewport(0, 0, width, height);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
	glUseProgram(0);
}