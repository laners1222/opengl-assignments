#include "scene_texture.h"

#include "ifile.h"
#include "time.h"
#include "vec3.h"
#include "vec2.h"
#include "mat4.h"
#include "mat3.h"

#include <vector>




scene_texture::~scene_texture()
{
	glDeleteProgram(shader_program);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXT);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXT2);
}

void scene_texture::init()
{
	

	std::vector<cgmath::vec3> positions;
	//Front
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f));   //0
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f));  //1
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f)); //2
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));  //3

	//Up
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));

	//Right
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));

	//Left
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));

	//Back
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));

	//Down
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));

	std::vector<unsigned int> indices{ 1, 2, 0, 3, 0, 2,
									   6, 5, 4, 7, 5, 6,
									   9, 10, 8, 11, 10, 9,
									   15, 13, 12, 14, 15, 12,
									   19, 17, 18, 18, 17, 16,
									   22, 20, 23, 20, 21, 23 };

	std::vector<cgmath::vec3> normals;
	//Front
	normals.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));

	//Up
	normals.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));

	//Right
	normals.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));

	//Left
	normals.push_back(cgmath::vec3(-1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(-1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(-1.0f, 0.0f, 0.0f));
	normals.push_back(cgmath::vec3(-1.0f, 0.0f, 0.0f));

	//Back
	normals.push_back(cgmath::vec3(0.0f, 0.0f, -1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, -1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, -1.0f));
	normals.push_back(cgmath::vec3(0.0f, 0.0f, -1.0f));

	//Down
	normals.push_back(cgmath::vec3(0.0f, -1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, -1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, -1.0f, 0.0f));
	normals.push_back(cgmath::vec3(0.0f, -1.0f, 0.0f));

	std::vector<cgmath::vec2> coords;
	//Front
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 0.0f));

	//Up
	coords.push_back(cgmath::vec2(1.0f, 0.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));

	//Right
	coords.push_back(cgmath::vec2(1.0f, 0.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));

	//Left
	coords.push_back(cgmath::vec2(1.0f, 0.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));

	//Back
	coords.push_back(cgmath::vec2(1.0f, 0.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));

	//Down
	coords.push_back(cgmath::vec2(1.0f, 0.0f));
	coords.push_back(cgmath::vec2(0.0f, 0.0f));
	coords.push_back(cgmath::vec2(1.0f, 1.0f));
	coords.push_back(cgmath::vec2(0.0f, 1.0f));

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3)*positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normalsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3)*normals.size(), normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, coordsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2)*coords.size(), coords.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_IND);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VERTEX_BUFFER_O_IND);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);

	ifile shader_file;

	shader_file.read("shaders/texture.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/texture.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("textures/pig.png");

	glGenTextures(1, &VERTEX_BUFFER_O_TEXT);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
	ilBindImage(0);
	ilDeleteImage(1);

	ilGenImages(1, &imageID2);
	ilBindImage(imageID2);
	ilLoadImage("textures/crate.png");

	glGenTextures(1, &VERTEX_BUFFER_O_TEXT2);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT2);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
	ilBindImage(0);
	ilDeleteImage(1);

	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "VertexNormal");
	glBindAttribLocation(shader_program, 2, "texCoords");
	glLinkProgram(shader_program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400, 400);

	GLuint light_color = glGetUniformLocation(shader_program, "LightColor");
	glUniform3f(light_color, 1., 1., 1.);

	GLuint light_location = glGetUniformLocation(shader_program, "LightPosition");
	glUniform3f(light_location, -10., 10., 10.);

	GLuint camera_location = glGetUniformLocation(shader_program, "CameraPosition");
	glUniform3f(camera_location, 0., 0., 10.);

	glUseProgram(0);
}

void scene_texture::awake()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
}

void scene_texture::sleep()
{
}

void scene_texture::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);

	float time = time::elapsed_time().count();

	cgmath::mat4 rotacional_xCube(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, cos(time * 0.523599), -sin(time * 0.523599), 0.0f),
		cgmath::vec4(0.0f, sin(time * 0.523599), cos(time * 0.523599), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	cgmath::mat4 rotyCube(cgmath::vec4(cos(time * 1.0472), 0.0f, sin(time * 1.0472), 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(-sin(time * 1.0472), 0.0f, cos(time * 1.0472), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	cgmath::mat4 zrothCube(cgmath::vec4(cos(time * 0.523599), -sin(time * 0.523599), 0.0f, 0.0f),
		cgmath::vec4(sin(time * 0.523599), cos(time * 0.523599), 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	cgmath::mat4 matModel = cgmath::mat4::transpose(zrothCube) * cgmath::mat4::transpose(rotyCube) * cgmath::mat4::transpose(rotacional_xCube);

	cgmath::mat3 matModel_3(matModel);

	cgmath::mat3 matNorm = cgmath::mat3::transpose(cgmath::mat3::inverse(matModel_3));

	cgmath::mat4 trasCam(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 10.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	cgmath::mat4 matView = cgmath::mat4::inverse(cgmath::mat4::transpose(trasCam));

	cgmath::mat4 matProject(cgmath::vec4((1.0f / (1.0f * tan(1.0472 / 2))), 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, (1.0f / tan(1.0472 / 2)), 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, -((1000.0f + 1.0f) / (1000.0f - 1.0f)), -((2.0f * 1000.0f * 1.0f) / (1000.0f - 1.0f))),
		cgmath::vec4(0.0f, 0.0f, -1.0f, 0.0f));

	cgmath::mat4 matProjection = cgmath::mat4::transpose(matProject);

	GLuint model = glGetUniformLocation(shader_program, "modelMatrix");
	glUniformMatrix4fv(model, 1, GL_FALSE, &matModel[0][0]);

	GLuint view = glGetUniformLocation(shader_program, "viewMatrix");
	glUniformMatrix4fv(view, 1, GL_FALSE, &matView[0][0]);

	GLuint project = glGetUniformLocation(shader_program, "projectionMatrix");
	glUniformMatrix4fv(project, 1, GL_FALSE, &matProjection[0][0]);

	GLuint normal = glGetUniformLocation(shader_program, "normalMatrix");
	glUniformMatrix3fv(normal, 1, GL_FALSE, &matNorm[0][0]);

	GLint ubicacion_de_textura = glGetUniformLocation(shader_program, "texture1");
	glUniform1i(ubicacion_de_textura, 0);

	GLint ubicacion_de_textura2 = glGetUniformLocation(shader_program, "texture2");
	glUniform1i(ubicacion_de_textura2, 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT2);

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);
}

void scene_texture::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
	glUseProgram(0);
}