#include "scene_rain.h"
#include <stdio.h>
#include <stdlib.h>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>
#include <IL/il.h>
#include <iostream>

// struct

struct Gota {
	int id_gota;
	float espacio_gota;
};

//hlprs

int* generador_pool_rate(int a, int b, int states);
int next_id(int current_id, int step);
int pool_draught(int rtio, int id);
float aleatorio_en(float x, float q);
void reset_gota(int cuentagotas);
void refrescar_particula(float tiempo, float ralenti, int numi, int id);
int cofac_truncr(int a, int b, float c);
int comparador_sorter(const void * gota_superior, const void * gota_inferior);
void reset_gota(int cuentagotas);
void carnita_al_asador(float tamano);
void push_em_forward(int who, float posicion_x, float posicion_y, float width_pos);

//globales :
const int limite_particulas_global = 3000;
int i, j;
float lookdown_perspective = 0.0f;
float horizontal_perspective = 0.0f;
int part_lim_sup_gh = 600;
int part_lim_inf_gh = 500;
int* pool_food = generador_pool_rate(part_lim_inf_gh, part_lim_sup_gh, 60);
int comparador_sorter(const void * a, const void * b);
cgmath::vec3 vec_camara_posicion = cgmath::vec3(0.0f, 0.0f, 10.0f);
GLuint shader_program;
GLuint VERTEX_ARRAY_OBJECT;
GLuint VERTEX_ARRAY_OBJECT_POLIGONO;
GLuint VERTEX_ARRAY_OBJECT_PLANO;
GLuint VERTEX_BUFFER_O_PLNS1;
GLuint VERTEX_BUFFER_O_NORMAL;
GLuint wallVBO;
GLuint VERTEX_BUFFER_O_IND;
GLuint indicesWall;
GLuint VERTEX_BUFFER_O_TEXT;
GLuint VERTEX_BUFFER_O_TEXT2;
float aspect;
Gota mapa_camara_rays[limite_particulas_global];
std::vector<unsigned int> indices = { 0, 1, 2, 3, 0, 2, 3 };
std::vector<cgmath::vec3> vec_polygono;
std::vector<cgmath::vec3> refractariouwu;
std::vector<cgmath::vec3> mapa_gotas;
std::vector<cgmath::vec3> mapa_velspeed;
std::vector<cgmath::vec2> vec_planos;
std::vector<GLfloat> mapa_duracion;
std::vector<GLfloat> mapa_muerte;
float lejos_dist = 1000.0f;
float cerca_dist = 0.10f;
float ojo_angular = cgmath::radians(25.0f);
float magnitud_viento;
float magnitud_velocidad_global;
bool siendo_configurado;
int flecha_pool;

scene_rain::~scene_rain(){
	glDeleteProgram(shader_program);
	glDeleteVertexArrays(1, &VERTEX_ARRAY_OBJECT);
	glDeleteBuffers(1, &VERTEX_ARRAY_OBJECT_POLIGONO);
	glDeleteBuffers(1, &VERTEX_BUFFER_O_IND);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXT);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXT2);
}

void scene_rain::resize(int width, int height) {
	glViewport(0, 0, width, height);
	aspect = static_cast<float>(width) / static_cast<float>(height);
}

void scene_rain::awake() {
	glClearColor(0.0125f, 0.0f, 0.05f, 0.0f);
	glPointSize(10.0f);
}

void scene_rain::sleep() {
	glPointSize(5.0f);
}

void scene_rain::normalKeysDown(unsigned char key){
	if (key == 's'){
		lookdown_perspective -= 1.15f;
		return;
	}
	if (key == 'w'){
		lookdown_perspective += 1.15f;
		return;
	}
	if (key == 'a'){
		horizontal_perspective -= 1.15f;
		return;
	}
	if (key == 'd'){
		horizontal_perspective += 1.15f;
		return;
	}
	if (key == 'i' && magnitud_viento > -0.1f){
		magnitud_viento -= 0.005f;
		return;
	}
	if (key == 'p' && magnitud_viento < 0.1f){
		magnitud_viento += 0.005f;
		return;
	}
	if (key == 'o' && magnitud_velocidad_global < 2.8f){
		magnitud_velocidad_global += 0.2f;
		return;
	}
	if (key == 'l' && magnitud_velocidad_global > 0.7f){
		magnitud_velocidad_global -= 0.4f;
		return;
	}
	if (key == ' '){
		lookdown_perspective = 0.0f;
		horizontal_perspective = 0.0f;
		magnitud_viento = 0.005f;
		magnitud_velocidad_global = 1.0f;
		return;
	}
}

void scene_rain::refrescar_sistema() {
	float tiempo = time::delta_time().count();
	float ralenti = (-9.80f * tiempo) * 20;
	for (int j = 0; j < limite_particulas_global; j++) {
		if (mapa_duracion[mapa_camara_rays[j].id_gota] > 0.0f) {
			refrescar_particula(tiempo, ralenti, j, mapa_camara_rays[j].id_gota);
		}
		mapa_camara_rays[j].espacio_gota = cgmath::vec3::magnitude(mapa_gotas[mapa_camara_rays[j].id_gota] - vec_camara_posicion);
	}
	qsort(mapa_camara_rays, limite_particulas_global, sizeof(Gota), comparador_sorter);
}

//POOL
void scene_rain::reciclar_particulas(int razon_pool) {
	int id_gota = 0;
	for (int i = 0; i < limite_particulas_global; i++) {
		int consu = pool_draught(razon_pool, id_gota);
		if (consu) {
			break;
		}
		else if (mapa_duracion[i] <= 0.0f) {
			reset_gota(i);
			id_gota = next_id(id_gota, 1);
		}
	}
}

void scene_rain::init(){
	ILuint imageID;
	magnitud_viento = 0.003f;
	magnitud_velocidad_global = 1.0f;
	siendo_configurado = false;
	flecha_pool = 1;
	aspect = 100000.0f;
	for (i = 0; i < limite_particulas_global; i++){
		float X_M_U = 19000.5f;
		float posicion_x = X_M_U + aleatorio_en(X_M_U / 10.0f, X_M_U / 10.0f);
		float posicion_y = cofac_truncr(63, 12, 5.0f) + aleatorio_en(6.0f, 5.0f); 
		float width_pos = 1000000.0f + aleatorio_en(1.5f, 1.0f); 
		push_em_forward(i, posicion_x, posicion_y, width_pos);
	}
	carnita_al_asador(1.24f);
	glGenVertexArrays(1, &VERTEX_ARRAY_OBJECT);
	glBindVertexArray(VERTEX_ARRAY_OBJECT);
	glGenBuffers(1, &VERTEX_ARRAY_OBJECT_POLIGONO);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_ARRAY_OBJECT_POLIGONO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * vec_polygono.size(),vec_polygono.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &VERTEX_ARRAY_OBJECT_PLANO);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_ARRAY_OBJECT_PLANO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * vec_planos.size(),vec_planos.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &VERTEX_BUFFER_O_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_NORMAL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * refractariouwu.size(),refractariouwu.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &VERTEX_BUFFER_O_IND);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VERTEX_BUFFER_O_IND);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(),indices.data(), GL_STATIC_DRAW);
	glBindVertexArray(0);
	
	
	ifile shader_file;
	shader_file.read("shaders/lluvia.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);
	shader_file.read("shaders/flluvia.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);
	

	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/water.png");
	glGenTextures(1, &VERTEX_BUFFER_O_TEXT);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);

	
	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexSides");
	glBindAttribLocation(shader_program, 1, "Coordinates");
	glBindAttribLocation(shader_program, 2, "Normal");
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLinkProgram(shader_program);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);

}



void scene_rain::mainLoop()
{	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	float radianes_horizontales = cgmath::radians(lookdown_perspective);
	float radianes_verticales = cgmath::radians(horizontal_perspective);
	cgmath::vec4 rowxIDEti(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rowxyIDEti(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 fila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotfila_x_matriz(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotacional_plano_xy(0.0f, cos(radianes_horizontales), sin(radianes_horizontales), 0.0f);
	cgmath::vec4 rotacional_plano_xz(0.0f, -sin(radianes_horizontales), cos(radianes_horizontales), 0.0f);
	cgmath::vec4 rotacional_plano_xw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotacional_x(rotfila_x_matriz, rotacional_plano_xy, rotacional_plano_xz, rotacional_plano_xw);
	cgmath::vec4 ultimate_rot(cos(radianes_verticales), -sin(radianes_verticales), 0.0f, 0.0f);
	cgmath::vec4 mass_rot(sin(radianes_verticales), cos(radianes_verticales), 0.0f, 0.0f);
	cgmath::vec4 rotfila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 zrothink(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 zroth(ultimate_rot, mass_rot, rotfila_z_matriz, zrothink);
	cgmath::mat4 matriz_vista(1.0f);
	matriz_vista[3][0] = 0.0f;
	matriz_vista[3][1] = 0.0f;
	matriz_vista[3].z = 18.0f;
	matriz_vista = zroth * rotacional_x * cgmath::mat4::inverse(matriz_vista);
	cgmath::mat4 matriz_proyeccion;
	matriz_proyeccion[0][0] = 1.0f / (aspect * tan(ojo_angular));
	matriz_proyeccion[1][1] = 1.0f / tan(ojo_angular);
	matriz_proyeccion[2][2] = -((lejos_dist + cerca_dist) / (lejos_dist - cerca_dist));
	matriz_proyeccion[2][3] = -1.0f;
	matriz_proyeccion[3][2] = -((2 * lejos_dist * cerca_dist) / (lejos_dist - cerca_dist));
	cgmath::mat4 trans1(rowxIDEti, rowxIDEti, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	glUseProgram(shader_program);
	reciclar_particulas(pool_food[flecha_pool]);
	cgmath::mat4 singularidad(rowxIDEti, rowxyIDEti, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	GLuint v_location = glGetUniformLocation(shader_program,"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &matriz_vista[0][0]);
	GLuint p_location = glGetUniformLocation(shader_program,"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &matriz_proyeccion[0][0]);
	GLint ubicacion_de_textura =glGetUniformLocation(shader_program, "texture1");
	glUniform1i(ubicacion_de_textura, 0);
	GLuint lightColor = glGetUniformLocation(shader_program,"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);
	GLuint lightPosition = glGetUniformLocation(shader_program,"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);
	GLuint cameraPosition = glGetUniformLocation(shader_program,"vec_camara_posicion");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 0.0f);
	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_program,"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXT);
	glBindVertexArray(VERTEX_ARRAY_OBJECT);
	for (int j = 0; j < limite_particulas_global; j++){		
		int i = mapa_camara_rays[j].id_gota;
		if (mapa_duracion[i] > 0.0f){
			singularidad[3][0] = mapa_gotas[i].x;
			singularidad[3][1] = mapa_gotas[i].y;
			singularidad[3][2] = mapa_gotas[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_program,"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);
			GLuint trans_location = glGetUniformLocation(shader_program,"singularidad");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &singularidad[0][0]);			
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	refrescar_sistema();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

void carnita_al_asador(float tamano) {
	vec_planos.push_back(cgmath::vec2(0.0f, 0.0f));
	vec_planos.push_back(cgmath::vec2(1.0f, 0.0f));
	vec_planos.push_back(cgmath::vec2(1.0f, 1.0f));
	vec_planos.push_back(cgmath::vec2(0.0f, 1.0f));
	vec_polygono.push_back(tamano * cgmath::vec3(-0.0f, -0.0f, 0.0f));
	vec_polygono.push_back(tamano * cgmath::vec3(-0.03f, -0.08f, 0.0f));
	vec_polygono.push_back(tamano * cgmath::vec3(0.0f, -0.09f, 0.0f));
	vec_polygono.push_back(tamano * cgmath::vec3(0.03f, -0.08, 0.0f));
	vec_polygono.push_back(tamano * cgmath::vec3(-0.0f, -0.0f, 0.0f));
	refractariouwu.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	refractariouwu.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	refractariouwu.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	refractariouwu.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
}

int* generador_pool_rate(int a, int b, int states) {
	int arr[100];
	for (i = 0; i < states; i++) {
		arr[i] = a - 40 * i + 200 * (states - i);
	}
	arr[5] = b;
	return arr;
}

int next_id(int current_id, int step) {
	return current_id + step;
}

int pool_draught(int rtio, int id) {
	return id == rtio;
}

void refrescar_particula(float tiempo, float ralenti, int numi, int id) {
	float prevVel = mapa_velspeed[id].y;
	mapa_velspeed[id].y += ralenti / (1.54 * mapa_camara_rays[numi].espacio_gota);
	mapa_gotas[id].y += magnitud_velocidad_global * ((mapa_velspeed[id].y + prevVel) / 2.0f) * tiempo;
	mapa_gotas[id].x -= magnitud_viento;
	mapa_duracion[id] -= tiempo * magnitud_velocidad_global;
}

void push_em_forward(int who, float posicion_x, float posicion_y, float width_pos) {
	mapa_gotas.push_back(cgmath::vec3(posicion_x, posicion_y, width_pos));
	mapa_velspeed.push_back(cgmath::vec3(0.0f, -0.5f - aleatorio_en(0.1f, 0.5f), 0.0f));
	mapa_camara_rays[who].id_gota = who;
	mapa_camara_rays[who].espacio_gota = rand()*cgmath::vec3::magnitude(vec_camara_posicion - mapa_gotas[who]);
	mapa_duracion.push_back(2.0f + aleatorio_en(5.0f, 4.0f));
}

float aleatorio_en(float x, float q) {
	return static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (x + q)));
}

int cofac_truncr(int a, int b, float c) {
	return (i % a) % b + c;
}

int comparador_sorter(const void * gota_superior, const void * gota_inferior) {
	Gota *ptra, *ptrb;
	ptra = (Gota *)gota_superior;
	ptrb = (Gota *)gota_inferior;
	return (ptrb->espacio_gota - ptra->espacio_gota);
}

void reset_gota(int cuentagotas) {
	mapa_duracion[cuentagotas] = 3.5f + aleatorio_en(4.5f, 3.5f);
	float nuex = -5.5f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (5.5f + 5.5f)));
	float nuey = i % 50 + 7.0f + (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (8.5f - 7.0f))));
	float nuez = -5.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (5.0 + 5.0f)));
	mapa_gotas[cuentagotas] = cgmath::vec3(nuex, nuey, nuez);
	mapa_velspeed[cuentagotas].y = 0.0007f + aleatorio_en(0.0013f, 0.0007f);
}