#include "scene_fire.h"
#include <stdio.h>
#include <stdlib.h>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>
#include <IL/il.h>
#include <iostream>

int TASE_DE_OSCILACIONES2 = 180;
const int limite_brasas = 15000;
int* generar_reticula(int a) {
	int arr[100];
	int step = (int)((a) / 50);
	for (int i = 0; i < 50; i++) {
		arr[i] = i * step + a;
	}
	for (int i = 51; i < 100; i++) {
		arr[i] = (50 * step) + (i * step);
	}
	return arr;
}
int* tasa_pool_arr = generar_reticula(10000);
struct Brasa {
	int id_flama;
	float optica_flama;
};
Brasa mapa_flamas[limite_brasas];
int ordenar_las_brasas(const void * a, const void * b){
	Brasa *orderA = (Brasa *)a;
	Brasa *orderB = (Brasa *)b;
	return (orderB->optica_flama - orderA->optica_flama);
}
cgmath::vec3 ojo_fuego = cgmath::vec3(0.0f, 0.0f, 10.0f);
float rotacional_horiz = -30.0f;
float pov_z = 0.0f;
float viento_externo;
float mechero;
int bunsen;
GLuint shader_programFire;
GLuint vaoFire;
GLuint vao1Fire;
GLuint VERTEX_BUFFER_O_POLYFire;
GLuint VERTEX_BUFFER_O_PLNSFire;
GLuint VERTEX_BUFFER_O_NORMALFire;
GLuint VERTEX_BUFFER_O_INDFire;
GLuint VERTEX_BUFFER_O_TEXTFire;
float dimts;
int INSANITY_FACTOR = 2;
std::vector<cgmath::vec3> brasero;
std::vector<cgmath::vec3> mapa_posicional;
std::vector<cgmath::vec3> intensidad_brasero_mapa;
std::vector<cgmath::vec2> plano_flamas;
std::vector<cgmath::vec3> poligono_flama;
std::vector<GLfloat> ciclo_vida_brasa;
std::vector<unsigned int> esquinas = { 0, 1, 2, 0, 2, 3 };

scene_fire::~scene_fire(){
	glDeleteProgram(shader_programFire);
	glDeleteVertexArrays(1, &vaoFire);
	glDeleteVertexArrays(1, &vao1Fire);
	glDeleteBuffers(1, &VERTEX_BUFFER_O_POLYFire);
	glDeleteBuffers(1, &VERTEX_BUFFER_O_INDFire);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXTFire);
}

void scene_fire::resize(int width, int height) {
	glViewport(0, 0, width, height);

	dimts = static_cast<float>(width) / static_cast<float>(height);
}

void scene_fire::awake() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(10.0f);
}

void scene_fire::sleep() {
	glPointSize(5.0f);
}

void prendemos_el_carbon(float scattered) {
	plano_flamas.push_back(cgmath::vec2(0.0f, 0.0f));
	plano_flamas.push_back(cgmath::vec2(1.0f, 0.0f));
	plano_flamas.push_back(cgmath::vec2(1.0f, 1.0f));
	plano_flamas.push_back(cgmath::vec2(0.0f, 1.0f));

	poligono_flama.push_back(scattered * cgmath::vec3(-0.05f, -0.16f, 0.5f));
	poligono_flama.push_back(scattered * cgmath::vec3(0.05f, -0.16f, 0.5f));
	poligono_flama.push_back(scattered * cgmath::vec3(0.05f, 0.16f, 0.5f));
	poligono_flama.push_back(scattered * cgmath::vec3(-0.05f, 0.16f, 0.5f));

	brasero.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	brasero.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	brasero.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	brasero.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
}

void scene_fire::normalKeysDown(unsigned char key) {
	if (key == 'a') {
		rotacional_horiz -= 20.0f;
		return;
	}
	if (key == 'd' && rotacional_horiz < 360.0f) {
		rotacional_horiz += 20.0f;
		return;
	}
	if (key == 's') {
		pov_z -= 20.0f;
		return;
	}
	if (key == 'w') {
		pov_z += 20.0f;
		return;
	}
	if (key == 'p' && viento_externo > -0.012f) {
		viento_externo -= 0.003f;
		return;
	}
	if (key == 'i' && viento_externo < 0.012f) {
		viento_externo += 0.003f;
		return;
	}
	if (key == 'o' && mechero < 1.8f) {
		mechero += 0.2f;
		return;
	}
	if (key == 'l' && mechero > 0.3f) {
		mechero -= 0.2f;
		return;
	}
}


void scene_fire::init(){
	bunsen = 2;
	dimts = 1.0f;
	ILuint imageID;
	prendemos_el_carbon(1);
	viento_externo = 0.010f;
	mechero = 0.7f * INSANITY_FACTOR;

	for (int i = 0; i < limite_brasas; i++){
		mapa_posicional.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
		intensidad_brasero_mapa.push_back(cgmath::vec3(0.0f, -1 * 0.0005f + static_cast <float> (rand()) /	(static_cast <float> (RAND_MAX / (0.001f - 0.0005f))), 0.0f));
		mapa_flamas[i].id_flama = i;
		mapa_flamas[i].optica_flama = cgmath::vec3::magnitude(ojo_fuego -	mapa_posicional[i]);
		ciclo_vida_brasa.push_back(0.0f);
	}

	glGenVertexArrays(1, &vaoFire);
	glBindVertexArray(vaoFire);

	glGenBuffers(1, &VERTEX_BUFFER_O_POLYFire);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_POLYFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * poligono_flama.size(),
		poligono_flama.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_PLNSFire);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_PLNSFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * plano_flamas.size(),
		plano_flamas.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_NORMALFire);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_NORMALFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * brasero.size(),
		brasero.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_INDFire);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VERTEX_BUFFER_O_INDFire);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * esquinas.size(),
		esquinas.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/fuego.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/ffuego.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/ember.png");
	glGenTextures(1, &VERTEX_BUFFER_O_TEXTFire);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXTFire);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_programFire = glCreateProgram();
	glAttachShader(shader_programFire, vertex_shader);
	glAttachShader(shader_programFire, fragment_shader);
	glBindAttribLocation(shader_programFire, 0, "VertexSides");
	glBindAttribLocation(shader_programFire, 1, "Coordinates");
	glBindAttribLocation(shader_programFire, 2, "Normal");

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	glLinkProgram(shader_programFire);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);

}

void ahhdejalo_reposar() {
	if (TASE_DE_OSCILACIONES2 == 0) {
		viento_externo = (viento_externo * -0.97);
		TASE_DE_OSCILACIONES2 = 30;
	}
	else TASE_DE_OSCILACIONES2 -= 1;
}

void check_pos_map(int i) {
	if (mapa_posicional[i].x < 0.0f) mapa_posicional[i].x += 0.001 * abs(mapa_posicional[i].x);
	else mapa_posicional[i].x -= 0.04 * abs(mapa_posicional[i].x);
}

void actualiza_brasa(int currwent_id, float acumulado, float tiempo, float ralenti) {
	if (ciclo_vida_brasa[currwent_id] > 0.0f) {
		float vel_anterior = intensidad_brasero_mapa[currwent_id].y;
		intensidad_brasero_mapa[currwent_id].y += ralenti;
		mapa_posicional[currwent_id].y += (mechero * ((intensidad_brasero_mapa[currwent_id].y + vel_anterior) / 2.0f) * tiempo);
		check_pos_map(currwent_id);
		mapa_posicional[currwent_id].x -= (4 * viento_externo + sin(acumulado) * 0.005f);
		ciclo_vida_brasa[currwent_id] -= tiempo * mechero;
	}
}

void scene_fire::calentar_llamas() {
	float tiempo = time::delta_time().count();
	float acumulado = time::elapsed_time().count();
	float ralenti = -4.905f* 0.66*INSANITY_FACTOR * tiempo;
	ahhdejalo_reposar();
	for (int j = 0; j < limite_brasas; j++) {
		int currwent_id = mapa_flamas[j].id_flama;
		actualiza_brasa(currwent_id, acumulado, tiempo, ralenti);
		mapa_flamas[j].optica_flama = cgmath::vec3::magnitude(mapa_posicional[currwent_id]- ojo_fuego);
	}
	qsort(mapa_flamas, limite_brasas, sizeof(Brasa), ordenar_las_brasas);
}

int generar_brasa_flama(int k) {
	if (ciclo_vida_brasa[k] <= 0.0f){
		ciclo_vida_brasa[k] = 1.0f + static_cast <float> (rand()) /(static_cast <float> (RAND_MAX / (2.5f - 1.0f)));
		float tx = -2.0f + static_cast <float> (rand()) /(static_cast <float> (RAND_MAX / (2.0f + 2.0f)));
		float ty = -4.0f + (static_cast <float> (rand()) /(static_cast <float> (RAND_MAX / (-3.0f + 4.0f))));
		float tz = -5.0f + static_cast <float> (rand()) /(static_cast <float> (RAND_MAX / (5.0 + 5.0f)));
		mapa_posicional[k] = cgmath::vec3(tx, ty, tz);
		intensidad_brasero_mapa[k].y = 7.5f + static_cast <float>
			(rand()) / (static_cast <float> (RAND_MAX / (9.6f - 7.5f)));
		return 1;
	}
	else {
		return 0;
	}
}

void scene_fire::pool_refresh_flama(int pool_rate){
	int id_flama = 0;
	int count = 0; 
	while (count < limite_brasas && id_flama != pool_rate){
		id_flama = id_flama + generar_brasa_flama(count);
		count = count + 1;
	}
}

void scene_fire::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	float aX = cgmath::radians(rotacional_horiz);
	float aZ = cgmath::radians(pov_z);
	cgmath::vec4 fila_x_matriz(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 fila_y_matriz(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 fila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotfila_x_matriz(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotacional_plano_xy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotacional_plano_xz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotacional_plano_xw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotacional_x(rotfila_x_matriz, rotacional_plano_xy, rotacional_plano_xz, rotacional_plano_xw);
	cgmath::vec4 ultimate_rot(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 mass_rot(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotfila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 zrothink(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 zroth(ultimate_rot, mass_rot, rotfila_z_matriz, zrothink);

	cgmath::mat4 matriz_vista(1.0f);
	matriz_vista[3][0] = 0.0f;
	matriz_vista[3][1] = 0.0f;
	matriz_vista[3].z = 10.0f;
	matriz_vista = zroth * rotacional_x * cgmath::mat4::inverse(matriz_vista);

	float lejos_dist_part3 = 1000.0f;
	float cerca_dist_part3 = 1.0f;
	float ojo_angular = cgmath::radians(30.0f);

	cgmath::mat4 matriz_proyeccion;
	matriz_proyeccion[0][0] = 1.0f / (dimts * tan(ojo_angular));
	matriz_proyeccion[1][1] = 1.0f / tan(ojo_angular);
	matriz_proyeccion[2][2] = -((lejos_dist_part3 + cerca_dist_part3) / (lejos_dist_part3 - cerca_dist_part3));
	matriz_proyeccion[2][3] = -1.0f;
	matriz_proyeccion[3][2] = -((2 * lejos_dist_part3 * cerca_dist_part3) / (lejos_dist_part3 - cerca_dist_part3));

	cgmath::mat4 trans1(fila_x_matriz, fila_y_matriz, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	glUseProgram(shader_programFire);
	pool_refresh_flama(tasa_pool_arr[bunsen]);
	cgmath::mat4 singularidad(fila_x_matriz, fila_y_matriz, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	GLuint v_location = glGetUniformLocation(shader_programFire,"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &matriz_vista[0][0]);
	GLuint p_location = glGetUniformLocation(shader_programFire,"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &matriz_proyeccion[0][0]);
	GLint ubicacion_de_textura =glGetUniformLocation(shader_programFire, "texture1");
	glUniform1i(ubicacion_de_textura, 0);
	GLuint lightColor = glGetUniformLocation(shader_programFire,"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);
	GLuint lightPosition = glGetUniformLocation(shader_programFire,"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);
	GLuint cameraPosition = glGetUniformLocation(shader_programFire,"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);
	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_programFire,	"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXTFire);
	glBindVertexArray(vaoFire);
	for (int j = 0; j < limite_brasas; j++){
		int i = mapa_flamas[j].id_flama;
		if (ciclo_vida_brasa[i] > 0.0f){
			singularidad[3][0] = mapa_posicional[i].x;
			singularidad[3][1] = mapa_posicional[i].y;
			singularidad[3][2] = mapa_posicional[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_programFire,"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);
			GLuint trans_location = glGetUniformLocation(shader_programFire,"singularidad");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &singularidad[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	calentar_llamas();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}



