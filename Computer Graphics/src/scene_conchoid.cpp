#include "scene_conchoid.h"
#include "vec2.h"
#include <vector>
#include <math.h>

void scene_conchoid::init(){
	std::vector<cgmath::vec2> LINEA_INFINITA;
	LINEA_INFINITA.push_back(cgmath::vec2(1.0f, 0.1f));
	LINEA_INFINITA.push_back(cgmath::vec2(-1.0f, 0.1f));
	count = LINEA_INFINITA.size();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &positionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * LINEA_INFINITA.size(), LINEA_INFINITA.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	std::vector<cgmath::vec2> PRIMER_TRAMO;
	std::vector<cgmath::vec2> PRIMER_TRAMO2;
	for (int i = 0; i < 200; ++i) {
		cgmath::vec2 P(LINEA_INFINITA[1].x, 0.1f);
		cgmath::vec2 O(0.0f, 0.0f);
		cgmath::vec2 V = O - P;
		V.normalize();
		cgmath::vec2 Q = P + (0.2f * V);
		cgmath::vec2 Q2 = P - (0.2f * V);
		PRIMER_TRAMO.push_back(Q);
		PRIMER_TRAMO2.push_back(Q2);
		LINEA_INFINITA[1].x += 0.01f;
	}
	count2 = PRIMER_TRAMO.size();	
	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);
	glGenBuffers(1, &positionVBO2);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * PRIMER_TRAMO.size(), PRIMER_TRAMO.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glGenVertexArrays(1, &vao3);
	glBindVertexArray(vao3);
	glGenBuffers(1, &positionVBO3);
	glBindBuffer(GL_ARRAY_BUFFER, positionVBO3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * PRIMER_TRAMO2.size(), PRIMER_TRAMO2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
void scene_conchoid::awake(){
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(20.0f);
}
void scene_conchoid::sleep(){
	glPointSize(1.0f);
}
void scene_conchoid::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindVertexArray(vao);
	glDrawArrays(GL_LINE_STRIP, 0, count);
	glBindVertexArray(0);
	glBindVertexArray(vao2);
	glDrawArrays(GL_LINE_STRIP, 0, count2);
	glBindVertexArray(0);
	glBindVertexArray(vao3);
	glDrawArrays(GL_LINE_STRIP, 0, count2);
	glBindVertexArray(0);
}
