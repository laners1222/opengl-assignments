#include "scene_snow.h"
#include <stdio.h>
#include <stdlib.h>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>
#include <IL/il.h>
#include <iostream>

const int limite_particulas_global = 3000;

struct Snowflake {
	int id_snowflake;
	float espacio_copo;
};

void zapatero_a_su_zapato();
float sharp_aleatorio_en(float x, float q);
float sharp_aleatorio_en(float x, float q) {
	return static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (x + q)));
}
int* generador_pool_rate_fur_snow(int a, int b, int states);

cgmath::vec3 mapa_camara_rays = cgmath::vec3(0.0f, 0.0f, 10.0f);

GLuint shader_programSnow;
GLuint wall_programSnow;
GLuint vaoSnow;
GLuint VERTEX_BUFFER_O_POLYSnow;
GLuint VERTEX_BUFFER_O_PLNSSnow;
GLuint VERTEX_BUFFER_O_NORMALSnow;
GLuint wallVBOSnow;
GLuint VERTEX_BUFFER_O_INDSnow;
GLuint indicesWallSnow;

GLuint VERTEX_BUFFER_O_TEXTSnow;
GLuint VERTEX_BUFFER_O_TEXT2Snow;
float dims;
// 1 = CADA ACTUALIZACION, 60 = 1 seg N = CADA N MLOOPS
int TASE_DE_OSCILACIONES = 180;
std::vector<cgmath::vec3> mapa_snowflake_tensors;
std::vector<cgmath::vec3> mapa_snowflake_volisida;
std::vector<cgmath::vec2> vec_copoplanoi;
std::vector<GLfloat> mapa_snowflake_feelings;
std::vector<cgmath::vec3> normals;
std::vector<cgmath::vec3> poligonales;
std::vector<unsigned int> hardcore_indts = { 0, 1, 2, 0, 2, 3 };

float horiz_copol = 0.0f;
float transversalidad = 0.0f;
float potencial_viento;
float poder_tormeta;
bool en_config;
int copo_flechero;
int* pool_junk_food = generador_pool_rate_fur_snow(-1, -1, 60);

Snowflake mapa_rayos_poderosos[limite_particulas_global];

int sort_em_good(const void * a, const void * b){
	Snowflake *fasilitaA = (Snowflake *)a;
	Snowflake *fasilitaB = (Snowflake *)b;

	return (fasilitaB->espacio_copo - fasilitaA->espacio_copo);
}

scene_snow::~scene_snow(){
	glDeleteProgram(shader_programSnow);
	glDeleteProgram(wall_programSnow);
	glDeleteVertexArrays(1, &vaoSnow);
	glDeleteBuffers(1, &VERTEX_BUFFER_O_POLYSnow);
	glDeleteBuffers(1, &VERTEX_BUFFER_O_INDSnow);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXTSnow);
	glDeleteTextures(1, &VERTEX_BUFFER_O_TEXT2Snow);
}

void zapatero_a_su_zapato(float escala) {
	float tormenta_perfecta=sharp_aleatorio_en(escala - 0.10f, escala + 0.10f);
	vec_copoplanoi.push_back(cgmath::vec2(0.0f, 0.0f));
	vec_copoplanoi.push_back(cgmath::vec2(1.0f, 0.0f));
	vec_copoplanoi.push_back(cgmath::vec2(1.0f, 1.0f));
	vec_copoplanoi.push_back(cgmath::vec2(0.0f, 1.0f));
	poligonales.push_back(tormenta_perfecta * cgmath::vec3(-0.14f, -0.14f, 0.5f));
	poligonales.push_back(tormenta_perfecta * cgmath::vec3(0.14f, -0.14f, 0.5f));
	poligonales.push_back(tormenta_perfecta * cgmath::vec3(0.14f, 0.14f, 0.5f));
	poligonales.push_back(tormenta_perfecta * cgmath::vec3(-0.14f, 0.14f, 0.5f));
	normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
}

void feel_the_love(int k) {
	for (int i = 0; i < limite_particulas_global; i++) {
		mapa_snowflake_tensors.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
		mapa_snowflake_volisida.push_back(cgmath::vec3(0.0f, k * 0.0005f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.001f - 0.0005f))), 0.0f));
		mapa_rayos_poderosos[i].id_snowflake = i;
		mapa_rayos_poderosos[i].espacio_copo = cgmath::vec3::magnitude(mapa_camara_rays - mapa_snowflake_tensors[i]);
		mapa_snowflake_feelings.push_back(0.0f);
	}
}

void scene_snow::init(){
	ILuint imageID;
	en_config = false;
	copo_flechero = 2;
	dims = 1.0f;
	zapatero_a_su_zapato(0.1);
	feel_the_love(-1);
	potencial_viento = 0.05f;
	poder_tormeta = 0.5f;

	glGenVertexArrays(1, &vaoSnow);
	glBindVertexArray(vaoSnow);

	glGenBuffers(1, &VERTEX_BUFFER_O_POLYSnow);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_POLYSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * poligonales.size(),
		poligonales.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_PLNSSnow);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_PLNSSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * vec_copoplanoi.size(),
		vec_copoplanoi.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_NORMALSnow);
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_BUFFER_O_NORMALSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normals.size(),
		normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VERTEX_BUFFER_O_INDSnow);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VERTEX_BUFFER_O_INDSnow);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * hardcore_indts.size(),
		hardcore_indts.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/hielo.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/fhielo.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/snowflake.png");
	glGenTextures(1, &VERTEX_BUFFER_O_TEXTSnow);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXTSnow);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_programSnow = glCreateProgram();
	glAttachShader(shader_programSnow, vertex_shader);
	glAttachShader(shader_programSnow, fragment_shader);
	glBindAttribLocation(shader_programSnow, 0, "VertexSides");
	glBindAttribLocation(shader_programSnow, 1, "Coordinates");
	glBindAttribLocation(shader_programSnow, 2, "Normal");

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLinkProgram(shader_programSnow);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);
}

void dejalo_reposar() {
	if (TASE_DE_OSCILACIONES == 0) {
		potencial_viento = potencial_viento * -0.97;
		TASE_DE_OSCILACIONES = 180;
	}
	else TASE_DE_OSCILACIONES -= 1;
}

void actualizar_copo(float tiempo, float acumulacion, float proverbial_ralenti, int j) {
	int nxt_dst = mapa_rayos_poderosos[j].id_snowflake;
	if (mapa_snowflake_feelings[nxt_dst] > 0.0f) {
		float vel_anterior = mapa_snowflake_volisida[nxt_dst].y;
		mapa_snowflake_volisida[nxt_dst].y += proverbial_ralenti;
		mapa_snowflake_tensors[nxt_dst].y += (poder_tormeta * ((mapa_snowflake_volisida[nxt_dst].y + vel_anterior) / 2.0f) * tiempo);
		mapa_snowflake_tensors[nxt_dst].x -= (potencial_viento + sin(acumulacion) * 0.014f);
		mapa_snowflake_feelings[nxt_dst] -= tiempo * poder_tormeta;
	}
	mapa_rayos_poderosos[j].espacio_copo = cgmath::vec3::magnitude(mapa_snowflake_tensors[nxt_dst] - mapa_camara_rays);
}

void refresco_particulas() {
	float tiempo = time::delta_time().count();
	float acumulacion = time::elapsed_time().count();
	float proverbial_ralenti = (-4.905f * tiempo) / (3.1416);
	for (int j = 0; j < limite_particulas_global; j++) {
		actualizar_copo(tiempo, acumulacion, proverbial_ralenti, j);
	}
}

void scene_snow::refrescar_sistema_configurable_nieve() {
	dejalo_reposar();
	refresco_particulas();
	qsort(mapa_rayos_poderosos, limite_particulas_global, sizeof(Snowflake), sort_em_good);
}

float rub_it_x() {
	return -6.5f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (6.5f + 6.5f)));
}

float rub_it_y(int chaotics) {
	return chaotics % 50 + 7.0f + (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (8.5f - 7.0f))));
}

float rub_it_z() {
	return -5.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (5.0 + 5.0f)));
}


int do_the_goddamn_job(int mirror) {
	if (mapa_snowflake_feelings[mirror] <= 0.0f) {
		mapa_snowflake_feelings[mirror] = 4.5f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (5.5f - 3.5f)));
		float sax = rub_it_x();
		float say = rub_it_y(mirror);
		float saz = rub_it_z();
		mapa_snowflake_tensors[mirror] = cgmath::vec3(sax, say, saz);
		mapa_snowflake_volisida[mirror].y = 0.0007f + static_cast <float>(rand()) / (static_cast <float> (RAND_MAX / (0.0013f - 0.0007f)));
		return 1;
	}
	else {
		return 0;
	}
}

void scene_snow::pool_particle_reloader_refresher(int tasa_pool){
	int id_snowflake = 0;
	int mirror = 0;
	while (mirror < limite_particulas_global && id_snowflake != tasa_pool){
		id_snowflake = id_snowflake + do_the_goddamn_job(mirror);
		mirror++;
	}
}

void scene_snow::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(wall_programSnow);
	

	float aX = cgmath::radians(horiz_copol);
	float aZ = cgmath::radians(transversalidad);


	cgmath::vec4 fila_x_matriz(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 fila_y_matriz(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 fila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);

	cgmath::vec4 rotfila_x_matriz(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotacional_plano_xy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotacional_plano_xz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotacional_plano_xw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotacional_x(rotfila_x_matriz, rotacional_plano_xy, rotacional_plano_xz, rotacional_plano_xw);

	cgmath::vec4 ultimate_rot(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 mass_rot(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotfila_z_matriz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 zrothink(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 zroth(ultimate_rot, mass_rot, rotfila_z_matriz, zrothink);

	// View Matrix
	cgmath::mat4 matriz_vista(1.0f);
	matriz_vista[3][0] = 0.0f;
	matriz_vista[3][1] = 0.0f;
	matriz_vista[3].z = 7.0f;
	matriz_vista = zroth * rotacional_x * cgmath::mat4::inverse(matriz_vista);

	// Projection Matrix
	float lejos_dist = 1000.0f;
	float cerca_dist = 1.0f;
	float angular = cgmath::radians(45.0f);

	cgmath::mat4 matriz_proyeccion;
	matriz_proyeccion[0][0] = 1.0f / (dims * tan(angular));
	matriz_proyeccion[1][1] = 1.0f / tan(angular);
	matriz_proyeccion[2][2] = -((lejos_dist + cerca_dist) / (lejos_dist - cerca_dist));
	matriz_proyeccion[2][3] = -1.0f;
	matriz_proyeccion[3][2] = -((2 * lejos_dist * cerca_dist) / (lejos_dist - cerca_dist));

	cgmath::mat4 trans1(fila_x_matriz, fila_y_matriz, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	


	glUseProgram(shader_programSnow);
	pool_particle_reloader_refresher(pool_junk_food[copo_flechero]);
	cgmath::mat4 singularidad(fila_x_matriz, fila_y_matriz, fila_z_matriz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location = glGetUniformLocation(shader_programSnow,
		"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &matriz_vista[0][0]);

	GLuint p_location = glGetUniformLocation(shader_programSnow,
		"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &matriz_proyeccion[0][0]);

	GLint ubicacion_de_textura =
		glGetUniformLocation(shader_programSnow, "texture1");
	glUniform1i(ubicacion_de_textura, 0);

	GLuint lightColor = glGetUniformLocation(shader_programSnow,
		"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_programSnow,
		"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);

	GLuint cameraPosition = glGetUniformLocation(shader_programSnow,
		"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);

	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_programSnow,
		"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, VERTEX_BUFFER_O_TEXTSnow);
	glBindVertexArray(vaoSnow);
	for (int j = 0; j < limite_particulas_global; j++){
		int i = mapa_rayos_poderosos[j].id_snowflake;
		if (mapa_snowflake_feelings[i] > 0.0f)
		{
			singularidad[3][0] = mapa_snowflake_tensors[i].x;
			singularidad[3][1] = mapa_snowflake_tensors[i].y;
			singularidad[3][2] = mapa_snowflake_tensors[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_programSnow,
				"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);

			GLuint trans_location = glGetUniformLocation(shader_programSnow,
				"singularidad");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &singularidad[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	refrescar_sistema_configurable_nieve();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void scene_snow::resize(int width, int height){
	glViewport(0, 0, width, height);

	dims = static_cast<float>(width) / static_cast<float>(height);
}

void scene_snow::awake(){
	glClearColor(0.75f, 0.75f, 0.75f, 1.0f);
	glPointSize(10.0f);
}

void scene_snow::sleep(){
	glPointSize(5.0f);
}

void scene_snow::normalKeysDown(unsigned char key){
	if (key == 'a'){
		horiz_copol -= 20.0f;
		return;
	}
	if (key == 'd'){
		horiz_copol += 20.0f;
		return;
	}
	 if (key == 's'){
		transversalidad -= 20.0f;
		return;
	}
	 if (key == 'w'){
		transversalidad += 20.0f;
		return;
	}
	 if (key == 'i'){
		potencial_viento -= 0.001f;
		return;
	}
	 if (key == 'p'){
		potencial_viento += 0.001f;
		return;
	}
	 if (key == 'o'){
		poder_tormeta += 0.002f;
		potencial_viento -= 0.005f;
		return;
	}
	 if (key == 'l'){
		poder_tormeta -= 0.002f;
		if (potencial_viento > -0.10f) {
			potencial_viento -= 0.005f;
		}
		return;
	}

}


int* generador_pool_rate_fur_snow(int a, int b, int states) {
	int arr[] = { 750, 450, 300, 450, 750, 1000, 750, 450, 300, 450, 750, 1000, 750, 450, 300, 450, 750, 1000,
	750, 450, 300, 450, 750, 1000,  750, 450, 300, 450, 750, 1000, 750, 450, 300, 450, 750, 1000,  750, 450, 300, 450, 750, 1000,
	750, 450, 300, 450, 750, 1000,  750, 450, 300, 450, 750, 1000,  750, 450, 300, 450, 750, 1000 };
	return arr;
}
