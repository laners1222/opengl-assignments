#include "scene_cube.h"
#include "ifile.h"
#include "time.h"
#include "vec3.h"
#include "mat4.h"
#include <vector>

scene_cube::~scene_cube(){
	glDeleteProgram(shader_program);
}

void scene_cube::init(){
	std::vector<cgmath::vec3> positions;
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f)); 
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f)); 
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f)); 
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));  
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f));
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f));
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f));
	std::vector<cgmath::vec3> colors;
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.8f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.8f, 0.0f));
	colors.push_back(cgmath::vec3(0.8f, 0.8f, 0.0f));
	colors.push_back(cgmath::vec3(0.8f, 0.8f, 0.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.2f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.4f));
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.8f));
	std::vector<unsigned int> indices{ 1, 2, 0, 3, 0, 2,
									   6, 5, 4, 7, 5, 6,
									   9, 10, 8, 11, 10, 9,
									   15, 13, 12, 14, 15, 12,
									   19, 17, 18, 18, 17, 16,
									   22, 20, 23, 20, 21, 23 };
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3)*positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &colorVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3)*colors.size(), colors.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &VERTEX_BUFFER_O_IND);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VERTEX_BUFFER_O_IND);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);
	glBindVertexArray(0);
	ifile shader_file;
	shader_file.read("shaders/defaultmove.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);
	shader_file.read("shaders/Default.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);
	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "VertexColor");
	glLinkProgram(shader_program);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400, 400);
	glUseProgram(0);
}

void scene_cube::awake(){
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void scene_cube::sleep(){}
void scene_cube::mainLoop(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader_program);
	float time = time::elapsed_time().count();
	float aspect = 1.0f;
	float treintaGradosRadianes = 0.523599;
	float sesentaGradosRadianes = 1.0472;
	cgmath::mat4 matrizRotacionX(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
					      cgmath::vec4(0.0f, cos(time * treintaGradosRadianes), -sin(time * treintaGradosRadianes), 0.0f),
				          cgmath::vec4(0.0f, sin(time * treintaGradosRadianes), cos(time * treintaGradosRadianes), 0.0f),
					      cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 matrizRotacionY(cgmath::vec4(cos(time * sesentaGradosRadianes), 0.0f, sin(time * sesentaGradosRadianes), 0.0f),
					      cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
					      cgmath::vec4(-sin(time * sesentaGradosRadianes), 0.0f, cos(time * sesentaGradosRadianes), 0.0f),
					      cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 matrizRotacionZ(cgmath::vec4(cos(time * treintaGradosRadianes), -sin(time * treintaGradosRadianes), 0.0f, 0.0f),
					      cgmath::vec4(sin(time * treintaGradosRadianes), cos(time * treintaGradosRadianes), 0.0f, 0.0f),
					      cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
					      cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 matrizModelo = cgmath::mat4::transpose(matrizRotacionZ) * cgmath::mat4::transpose(matrizRotacionY) * cgmath::mat4::transpose(matrizRotacionX);
	cgmath::mat4 matrizConfigCamara(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
						  cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
				          cgmath::vec4(0.0f, 0.0f, 1.0f, 10.0f),
				          cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 matrizVista = cgmath::mat4::inverse(cgmath::mat4::transpose(matrizConfigCamara));
	cgmath::mat4 matrizProyeccion(cgmath::vec4((1.0f /(aspect * tan(1.0472 / 2))), 0.0f, 0.0f, 0.0f),
						   cgmath::vec4(0.0f, (1.0f / tan(1.0472 / 2)), 0.0f, 0.0f),
						   cgmath::vec4(0.0f, 0.0f, -((1000.0f + 1.0f) / (1000.0f - 1.0f)), -((2.0f * 1000.0f * 1.0f) / (1000.0f - 1.0f))),
						   cgmath::vec4(0.0f, 0.0f, -1.0f, 0.0f));
	cgmath::mat4 matrizProyeccionion = cgmath::mat4::transpose(matrizProyeccion);
	GLuint model = glGetUniformLocation(shader_program, "modelMatrix");
	glUniformMatrix4fv(model, 1, GL_FALSE, &matrizModelo[0][0]);
	GLuint view = glGetUniformLocation(shader_program, "viewMatrix");
	glUniformMatrix4fv(view, 1, GL_FALSE, &matrizVista[0][0]);
	GLuint project = glGetUniformLocation(shader_program, "projectionMatrix");
	glUniformMatrix4fv(project, 1, GL_FALSE, &matrizProyeccionion[0][0]);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glUseProgram(0);
}
void scene_cube::resize(int width, int height){
	glViewport(0, 0, width, height);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
	float aspect = static_cast<float>(width) / static_cast<float>(height);
	glUseProgram(0);
}
