#version 330

out vec3 InterpolatedColor;
uniform float time;

mat4 rotY() {
  return mat4(
    cos(time*(2.71/3.1416) / 1.0), 0, -sin(time*(2.71/3.1416) / 1.0), 0,
    0, 1, 0, 0,
    sin(time*(2.71/3.1416) / 1.0), 0, cos(time*(2.71/3.1416) / 1.0), 0,
    0, 0, 0, 1);
}

mat4 rotacional_x() {
  return mat4(
    1, 0, 0, 0,
    0, cos(time*(3.71/3.1416) / 1.0), -sin(time*(3.71/3.1416) / 1.0), 0,
    0, sin(time*(3.71/3.1416) / 1.0), cos(time*(3.71/3.1416) / 1.0), 0,
    0, 0, 0, 1);
}

mat4 zroth() {
  return mat4(
    -cos(time*(2.71/3.1416) / 1.0), sin(time*(2.71/3.1416) / 1.0), 0, 0,
    -sin(time*(2.71/3.1416) / 1.0), -cos(time*(2.71/3.1416) / 1.0), 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1);
}

void main() {
  float pi2 = 2.0*3.14159;
  float v = gl_VertexID;
  float vertex = mod(v, 6.0);
  v = (v-vertex)/6.0;
  float a1 = mod(v, 32.0);
  v = (v-a1)/32.0;
  float a2 = v-8.0;
  a1 += mod(vertex, 2.0);
  a2 += vertex == 2.0 || vertex >= 4.0?1.0:0.0;
  a1 = a1 / 32.0 * pi2;
  a2 = a2 / 32.0 * pi2;
  vec3 pos = vec3(cos(a1) * cos(a2), sin(a2), sin(a1) * cos(a2));
  vec4 pos4 = vec4(pos,1);
  pos4 *= rotacional_x();
  pos4 *= rotY();
  pos4 *= zroth();
  gl_Position = vec4(pos4.x * 1.0, pos4.y, pos4.z * 0.5+0.5, 1.);
  gl_PointSize = 6.66666666666;
  InterpolatedColor = pos + vec3(0.266f, 0.146f, 0.67432f);
}