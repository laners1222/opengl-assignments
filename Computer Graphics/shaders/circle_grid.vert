#version 330

out vec3 InterpolatedColor;

uniform float time;

vec2 createCircle(float vertexId){
  float x = floor(gl_VertexID / 2.0) + mod(gl_VertexID, 2.0);
  float y = mod(gl_VertexID + 1.0, 2.0);
  
  float angle = x / 20.0 * radians(360.0);
  float radius = 1.5 - y;
  
  float u = radius * cos(angle);
  float v = radius * sin(angle);
  
  vec2 xy = vec2(u, v);
  return xy;  
}

void main() 
{ 
  int numeroDeVertex = 1920;
  float circleId = floor(gl_VertexID/(30.0*4.0));
  float numCircles = floor(numeroDeVertex/(30.0*4.0));
  
  float down = floor(sqrt(numCircles));
  float across = floor(numCircles / down);
  
  float x = mod(circleId, across);
  float y = floor(circleId / across);
  
  float u = x / (across - 1.0);
  float v = y / (across -1.0);
  
  float xOffset = cos(time + y * 0.6) * 0.3;
  float yOffset = cos(time + x * 0.9) * 0.3;
  
  float ux = u * 2.0 - 1.0 + xOffset;
  float vy = v * 2.0 - 1.0 + yOffset;
  
  vec2 xy = createCircle(gl_VertexID) * 0.1 + vec2(ux,vy)*0.7;
  
  gl_Position= vec4(xy , 0.0, 1.0);
  InterpolatedColor = vec3(cos(time), sin(time), 0.5678);
}