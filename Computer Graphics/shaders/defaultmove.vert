#version 330

in vec3 VertexPosition;
in vec3 VertexColor;

out vec3 InterpolatedColor;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
	gl_Position = (projectionMatrix * viewMatrix * modelMatrix) * vec4(VertexPosition, 1.0f);
	InterpolatedColor = VertexColor;
}	