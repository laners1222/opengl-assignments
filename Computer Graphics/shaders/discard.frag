#version 330
out vec4 FragColor;
uniform vec2 iResolution;
in vec3 InterpolatedColor;

void main() {
	vec2 posicion = gl_FragCoord.xy / iResolution.xy;
	vec2 centro = vec2(0.5, 0.5);
	float distanciaAlCentro = distance(posicion, centro);
	if(distanciaAlCentro < 0.25){
		discard;
	}
	FragColor = vec4(InterpolatedColor, 0.0);
}