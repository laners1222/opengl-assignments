#version 330

in vec3 InterpolatedColor;
in vec3 PixelPosition;
in vec3 PixelNormal;

uniform vec3 LightColor;
uniform vec3 LightPosition;
uniform vec3 CameraPosition;

uniform sampler2D texture1;
uniform sampler2D texture2;
in vec2 coordTex;

out vec4 FragColor;

void main()
{
	vec3 Ambiental =  LightColor * .1;

	vec3 L = LightPosition - PixelPosition;
	L = normalize(L);
	float PX = dot(L, PixelNormal);
	PX = clamp(PX, 0., 1.);
	vec3 Difusa = PX * LightColor;

	vec3 R = reflect(-L, PixelNormal);
	vec3 V = CameraPosition - PixelPosition;
	V = normalize(V);
	float CX = clamp(dot(R, V), 0., 1.);
	CX = pow(CX, 64);
	vec3 Especular = CX * LightColor;

	vec4 Textura = texture2D(texture1, coordTex);
	vec4 Textura2 = texture2D(texture2, coordTex);

	vec4 Mix = mix(Textura, Textura2, 0.5);

	vec3 phong = (Ambiental + Difusa +  Especular) * vec3(Mix);

	FragColor = vec4(phong, 1.0);
}