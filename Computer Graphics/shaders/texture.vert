#version 330

in vec3 VertexPosition;
in vec3 VertexColor;
in vec3 VertexNormal;
in vec2 texCoords;

out vec2 coordTex;

out vec3 InterpolatedColor;
out vec3 PixelPosition;
out vec3 PixelNormal;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

void main()
{
	PixelPosition = vec3(modelMatrix * vec4(VertexPosition, 1.0f));
	PixelNormal = normalMatrix * VertexNormal;
	PixelNormal = normalize(PixelNormal);

	coordTex = texCoords;

	gl_Position = (projectionMatrix * viewMatrix * modelMatrix) * vec4(VertexPosition, 1.0f);
	InterpolatedColor = VertexColor;
}	