#version 330

in vec3 VertexSides;
in vec3 VertexColor;
out vec3 CurrentColor;
uniform mat4 trans;
uniform mat4 view;
uniform mat4 projection;
in vec2 Coordinates;
out vec2 coord;
void main(){
	mat4 viewTrans = view * trans;
	viewTrans[0][0] = 1;
	viewTrans[1][1] = 1;
	viewTrans[2][2] = 1;
	viewTrans[0][1] = 0;
	viewTrans[0][2] = 0;
	viewTrans[1][0] = 0;
	viewTrans[1][2] = 0;
	viewTrans[2][0] = 0;
	viewTrans[2][1] = 0;
	coord = Coordinates;
	mat4 ModelViewProjectionMatrix = projection * viewTrans;
	CurrentColor = VertexColor;
	gl_Position = ModelViewProjectionMatrix * vec4(VertexSides, 1.0f);
}