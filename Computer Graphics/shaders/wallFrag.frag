#version 330

out vec4 FragColor;
uniform sampler2D texture1;
in vec2 coord;

void main()
{
	FragColor = texture2D(texture1, coord);
}