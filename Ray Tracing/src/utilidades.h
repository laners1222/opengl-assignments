//-------------------------------------------------------------------------------------------------
// Ray tracer. Carlos Rivero (A01371368). Basado en 'Ray Tracing in One Weekend' de Peter Shirley
//-------------------------------------------------------------------------------------------------

#ifndef UTILIDADESH
#define UTILIDADESH

struct registro_impactos;

#include <math.h>
#include <stdlib.h>
#include <random>

class material;

float returnRandomNum(){
	float lower_bound = 0.;
	float upper_bound = 1.;
    std::uniform_real_distribution<float> unif(lower_bound, upper_bound);
    std::default_random_engine re;
	float a_random_double = unif(re);
    return a_random_double;
}

class vector3D  {
public:
    inline vector3D() {

    }
    inline vector3D(float e0, float e1, float e2) {
         e[0] = e0; e[1] = e1; e[2] = e2;
     }
    inline float x() const {
        return e[0];
    }
    inline float y() const {
        return e[1];
    }
    inline float z() const {
        return e[2];
    }
    inline float r() const {
        return e[0];
    }
    inline float g() const {
        return e[1];
    }
    inline float b() const {
        return e[2];
    }

    inline const vector3D& operator+() const {
        return *this;
    }

    inline vector3D operator-() const {
        return vector3D(-e[0], -e[1], -e[2]);
    }

    inline float operator[](int i) const {
         return e[i];
     }

    inline float& operator[](int i) {
        return e[i];
    };

    inline vector3D& operator+=(const vector3D &v2);
    inline vector3D& operator-=(const vector3D &v2);
    inline vector3D& operator*=(const vector3D &v2);
    inline vector3D& operator/=(const vector3D &v2);
    inline vector3D& operator*=(const float t);
    inline vector3D& operator/=(const float t);

    float length() const {
        return sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]);
    }
    float squared_length() const {
         return e[0]*e[0] + e[1]*e[1] + e[2]*e[2];
     }
    void make_unit_vector();


    float e[3];
};



inline std::istream& operator>>(std::istream &is, vector3D &t) {
    is >> t.e[0] >> t.e[1] >> t.e[2];
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const vector3D &t) {
    os << t.e[0] << " " << t.e[1] << " " << t.e[2];
    return os;
}

inline void vector3D::make_unit_vector() {
    float k = 1.0 / sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]);
    e[0] *= k; e[1] *= k; e[2] *= k;
}

inline vector3D operator+(const vector3D &v1, const vector3D &v2) {
    return vector3D(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

inline vector3D operator-(const vector3D &v1, const vector3D &v2) {
    return vector3D(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

inline vector3D operator*(const vector3D &v1, const vector3D &v2) {
    return vector3D(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

inline vector3D operator/(const vector3D &v1, const vector3D &v2) {
    return vector3D(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2]);
}

inline vector3D operator*(float t, const vector3D &v) {
    return vector3D(t*v.e[0], t*v.e[1], t*v.e[2]);
}

inline vector3D operator/(vector3D v, float t) {
    return vector3D(v.e[0]/t, v.e[1]/t, v.e[2]/t);
}

inline vector3D operator*(const vector3D &v, float t) {
    return vector3D(t*v.e[0], t*v.e[1], t*v.e[2]);
}

inline float dot(const vector3D &v1, const vector3D &v2) {
    return v1.e[0] *v2.e[0] + v1.e[1] *v2.e[1]  + v1.e[2] *v2.e[2];
}

inline vector3D cross(const vector3D &v1, const vector3D &v2) {
    return vector3D( (v1.e[1]*v2.e[2] - v1.e[2]*v2.e[1]),
                (-(v1.e[0]*v2.e[2] - v1.e[2]*v2.e[0])),
                (v1.e[0]*v2.e[1] - v1.e[1]*v2.e[0]));
}


inline vector3D& vector3D::operator+=(const vector3D &v){
    e[0]  += v.e[0];
    e[1]  += v.e[1];
    e[2]  += v.e[2];
    return *this;
}

inline vector3D& vector3D::operator*=(const vector3D &v){
    e[0]  *= v.e[0];
    e[1]  *= v.e[1];
    e[2]  *= v.e[2];
    return *this;
}

inline vector3D& vector3D::operator/=(const vector3D &v){
    e[0]  /= v.e[0];
    e[1]  /= v.e[1];
    e[2]  /= v.e[2];
    return *this;
}

inline vector3D& vector3D::operator-=(const vector3D& v) {
    e[0]  -= v.e[0];
    e[1]  -= v.e[1];
    e[2]  -= v.e[2];
    return *this;
}

inline vector3D& vector3D::operator*=(const float t) {
    e[0]  *= t;
    e[1]  *= t;
    e[2]  *= t;
    return *this;
}

inline vector3D& vector3D::operator/=(const float t) {
    float k = 1.0/t;

    e[0]  *= k;
    e[1]  *= k;
    e[2]  *= k;
    return *this;
}

inline vector3D unit_vector(vector3D v) {
    return v / v.length();
}

class rayito_de_luz{
    public:
        rayito_de_luz() {}
        rayito_de_luz(const vector3D& a, const vector3D& b) {
            A = a;
            B = b;
        }
        vector3D origin() const {
            return A;
        }
        vector3D direction() const {
            return B;
        }
        vector3D point_at_parameter(float t) const {
            return A + t*B;
        }

        vector3D A;
        vector3D B;
};

struct registro_impactos
{
    float t;
    vector3D p;
    vector3D normal;
    material *mat_ptr;
};

class superficie  {
    public:
        virtual bool hit(const rayito_de_luz& r, float t_min, float t_max, registro_impactos& rec) const = 0;
};

//Superficie es 'hitable'
class ll_superficies: public superficie  {
    public:
        ll_superficies() {

        }
        ll_superficies(superficie **l, int n) {
            list = l;
            list_size = n;
        }

        virtual bool hit(const rayito_de_luz& r, float tmin, float tmax, registro_impactos& rec) const;

        superficie **list;
        int list_size;
};

bool ll_superficies::hit(const rayito_de_luz& r, float t_min, float t_max, registro_impactos& rec) const {
        registro_impactos temp_rec;
        bool hit_anything = false;
		float closest_so_far = t_max;
        for (int i = 0; i < list_size; i++) {
            if (list[i]->hit(r, t_min, closest_so_far, temp_rec)) {
                hit_anything = true;
                closest_so_far = temp_rec.t;
                rec = temp_rec;
            }
        }
        return hit_anything;
}

float schlick(float coseno, float ref_idx) {
    float r0 = (1-ref_idx) / (1+ref_idx);
    r0 = r0*r0;
    return r0 + (1-r0)*pow((1 - coseno),5);
}
//Refracta la luz sobre esfera
bool refract(const vector3D& v, const vector3D& n, float ni_over_nt, vector3D& refractado) {
    vector3D unitario = unit_vector(v);
    float punto = dot(unitario, n);
    float discriminante = 1.0 - ni_over_nt*ni_over_nt*(1-punto*punto);
    if (discriminante > 0) {
        refractado = ni_over_nt*(unitario - n*punto) - n*sqrt(discriminante);
        return true;
    }
    else
        return false;
}


vector3D reflect(const vector3D& v, const vector3D& n) {
     return v - 2*dot(v,n)*n;
}

//Obtiene un vector unitario al azar
vector3D random_in_unit_sphere() {
    vector3D p;
    do {
        p = 2.0*vector3D(float(rand())/RAND_MAX, float(rand())/RAND_MAX, float(rand())/RAND_MAX) - vector3D(1,1,1);
    } while (p.squared_length() >= 1.0);
    return p;
}


class material  {
    public:
        virtual bool scatter(const rayito_de_luz& r_in, const registro_impactos& rec, vector3D& atenuador, rayito_de_luz& disperso) const = 0;
};

//Clase con propiedades mate de material.
class matte : public material {
    public:
        matte(const vector3D& a) : albedo(a) {}
        virtual bool scatter(const rayito_de_luz& r_in, const registro_impactos& rec, vector3D& atenuador, rayito_de_luz& disperso) const  {
             vector3D target = rec.p + rec.normal + random_in_unit_sphere();
             disperso = rayito_de_luz(rec.p, target-rec.p);
             atenuador = albedo;
             return true;
        }

        vector3D albedo;
};

class metalico : public material {
    public:
        metalico(const vector3D& a, float f) : albedo(a) { if (f < 1) fufila_z_matriz = f; else fufila_z_matriz = 1; }
        virtual bool scatter(const rayito_de_luz& r_in, const registro_impactos& rec, vector3D& atenuador, rayito_de_luz& disperso) const  {
            vector3D reflejado = reflect(unit_vector(r_in.direction()), rec.normal);
            disperso = rayito_de_luz(rec.p, reflejado + fufila_z_matriz*random_in_unit_sphere());
            atenuador = albedo;
            return (dot(disperso.direction(), rec.normal) > 0);
        }
        vector3D albedo;
        float fufila_z_matriz;
};

class cristalino_dielectrico : public material {
    public:
        cristalino_dielectrico(float ri) : ref_idx(ri) {}
        virtual bool scatter(const rayito_de_luz& r_in, const registro_impactos& rec, vector3D& atenuador, rayito_de_luz& disperso) const  {
             vector3D vector_saliente;
             vector3D reflejado = reflect(r_in.direction(), rec.normal);
             float ni_over_nt;
             atenuador = vector3D(1.0, 1.0, 1.0);
             vector3D refractado;
             float prob;
             float coseno;
             if (dot(r_in.direction(), rec.normal) > 0) {
                  vector_saliente = -rec.normal;
                  ni_over_nt = ref_idx;
                  coseno = dot(r_in.direction(), rec.normal) / r_in.direction().length();
                  coseno = sqrt(1 - ref_idx*ref_idx*(1-coseno*coseno));
             }
             else {
                  vector_saliente = rec.normal;
                  ni_over_nt = 1.0 / ref_idx;
                  coseno = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
             }
             if (refract(r_in.direction(), vector_saliente, ni_over_nt, refractado))
                prob = schlick(coseno, ref_idx);
             else
                prob = 1.0;
             if (float(rand())/RAND_MAX < prob)
                disperso = rayito_de_luz(rec.p, reflejado);
             else
                disperso = rayito_de_luz(rec.p, refractado);
             return true;
        }

        float ref_idx;
};

vector3D random_in_unit_disk() {
    vector3D p;
    do {
        p = 2.0*vector3D(float(rand())/RAND_MAX, float(rand())/RAND_MAX,0) - vector3D(1,1,0);
    } while (dot(p,p) >= 1.0);
    return p;
}



#endif
