//-------------------------------------------------------------------------------------------------
// Ray tracer. Carlos Rivero (A01371368). Basado en 'Ray Tracing in One Weekend' de Peter Shirley
//-------------------------------------------------------------------------------------------------

#ifndef CAMARAH
#define CAMARAH

#include "utilidades.h"
#include <cstdlib>

class camera {
    public:

        camera(vector3D origen_camara, vector3D objetivo_camara, vector3D vector_objetivo, float vector_pov, float tamano, float apertura_camara, float d_enfoque) { // vector_pov is top to bottom in degrees
            radio_camara = apertura_camara / 2;
            float angulo = vector_pov* 3.1415926535 / 180;
            float alturaM = tan(angulo/2);
            float anchoM = tamano * alturaM;
            origen = origen_camara;
            vector_a = unit_vector(origen_camara - objetivo_camara);
            vector_b = unit_vector(cross(vector_objetivo, vector_a));
            v = cross(vector_a, vector_b);
            lower_left_corner = origen  - anchoM*d_enfoque*vector_b -alturaM*d_enfoque*v - d_enfoque*vector_a;
            horizontal = 2*anchoM*d_enfoque*vector_b;
            vertical = 2*alturaM*d_enfoque*v;
        }
        
        rayito_de_luz get_ray(float s, float t) {
            vector3D rd = radio_camara*random_in_unit_disk();
            vector3D offset = vector_b * rd.x() + v * rd.y();
            return rayito_de_luz(origen + offset, lower_left_corner + s*horizontal + t*vertical - origen - offset);
        }

        vector3D origen;
        vector3D lower_left_corner;
        vector3D horizontal;
        vector3D vertical;
        vector3D vector_b, v, vector_a;
        float radio_camara;
};
#endif
