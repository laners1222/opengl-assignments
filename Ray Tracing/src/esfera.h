//-------------------------------------------------------------------------------------------------
// Ray tracer. Carlos Rivero (A01371368). Basado en 'Ray Tracing in One Weekend' de Peter Shirley
//-------------------------------------------------------------------------------------------------

#ifndef ESFERAH
#define ESFERAH

#include "utilidades.h"

class sphere: public superficie  {
    public:
        sphere() {}
        sphere(vector3D cen, float r, material *m) : centro(cen), radius(r), mat_ptr(m)  {};
        virtual bool hit(const rayito_de_luz& r, float tmin, float tmax, registro_impactos& registro) const;
        vector3D centro;
        float radius;
        material *mat_ptr;
};

bool sphere::hit(const rayito_de_luz& r, float parametro_minimo, float parametro_maximo, registro_impactos& registro) const {
    vector3D distancia_al_centro = r.origin() - centro;
    float a = dot(r.direction(), r.direction());
    float b = dot(distancia_al_centro, r.direction());
    float c = dot(distancia_al_centro, distancia_al_centro) - radius*radius;
    float discriminante = b*b - a*c;
    if (discriminante > 0) {
        float temp = (-b - sqrt(discriminante))/a;
        if (temp < parametro_maximo && temp > parametro_minimo) {
            registro.t = temp;
            registro.p = r.point_at_parameter(registro.t);
            registro.normal = (registro.p - centro) / radius;
            registro.mat_ptr = mat_ptr;
            return true;
        }
        temp = (-b + sqrt(discriminante)) / a;
        if (temp < parametro_maximo && temp > parametro_minimo) {
            registro.t = temp;
            registro.p = r.point_at_parameter(registro.t);
            registro.normal = (registro.p - centro) / radius;
            registro.mat_ptr = mat_ptr;
            return true;
        }
    }
    return false;
}


#endif
