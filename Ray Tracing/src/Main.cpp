//-------------------------------------------------------------------------------------------------
// Ray tracer. Carlos Rivero (A01371368). Basado en 'Ray Tracing in One Weekend' de Peter Shirley
//-------------------------------------------------------------------------------------------------

#include <iostream>
#include "esfera.h"
#include "camara.h"
#include "utilidades.h"
#include "float.h"
#include "image_writer.h"
#include <stdlib.h>
#include <vector>


//------------------------------------------------------------------------------------------

superficie *generar_config_aleatoria();
vector3D generar_color(const rayito_de_luz& r, superficie *universo, int profundidad);

//------------------------------------------------------------------------------------------

int main() {

    int ancho_lienzo = 1200;
    int alto_lienzo = 900;
    int muestras_por_pixel = 100;
    superficie *concentrador[6];
    float R = cos(3.1415926535 /4);


    concentrador[0] = new sphere(vector3D(0,0,-1), 0.5, new matte(vector3D(0.1, 0.2, 0.5)));
    concentrador[1] = new sphere(vector3D(0,-100.5,-1), 100, new matte(vector3D(0.8, 3, 0.0)));
    concentrador[2] = new sphere(vector3D(1,0,-1), 0.5, new metalico(vector3D(0.8, 0.6, 0.2), 0.0));
    concentrador[3] = new sphere(vector3D(-1,0,-1), 0.5, new cristalino_dielectrico(1.5));
    concentrador[4] = new sphere(vector3D(-1,0,-1), -0.45, new cristalino_dielectrico(1.5));
    concentrador[5] = new sphere(vector3D(-1,0,-1), -0.45, new cristalino_dielectrico(1.5));
    superficie *universo = new ll_superficies(concentrador,6);
    universo = generar_config_aleatoria();
    vector3D origen_camara(25,3,11);
    vector3D objetivo_camara(0,0,0);
    float d_enfoque = 10.0;
    float apertura = 0.5;


    camera cam(origen_camara, objetivo_camara, vector3D(0,1,0), 20, float(ancho_lienzo)/float(alto_lienzo), apertura, d_enfoque);
	std::vector<unsigned char> colores_pixel;

	int cuenta_operaciones = 0;
	int total_operaciones = ancho_lienzo * alto_lienzo*muestras_por_pixel;

    for (int j = alto_lienzo-1; j >= 0; j--){
        for (int i = 0; i < ancho_lienzo; i++){
            vector3D color(0, 0, 0);
            for (int k=0; k < muestras_por_pixel; k++){

				std::cout << "Progreso actual: " << float(cuenta_operaciones)*100 / (total_operaciones) << "%\n";

                float u = float(i + float(rand())/RAND_MAX) / float(ancho_lienzo);
                float v = float(j + float(rand())/RAND_MAX) / float(alto_lienzo);

                rayito_de_luz r = cam.get_ray(u, v);
                vector3D p = r.point_at_parameter(2.0);
                color += generar_color(r, universo,0);

				cuenta_operaciones++;
            }
            color = color / float(muestras_por_pixel);
            color = vector3D( sqrt(color[0]), sqrt(color[1]), sqrt(color[2]) );
            int rojo = int(255.99*color[0]);
            int verde = int(255.99*color[1]);
            int azul = int(255.99*color[2]);

			colores_pixel.push_back(rojo);
			colores_pixel.push_back(verde);
			colores_pixel.push_back(azul);
			
        }	
    }
	image_writer::save_png("captura2.png", ancho_lienzo, alto_lienzo, 3, colores_pixel.data());
}

superficie *generar_config_aleatoria() {
    int n = 800;
    superficie **concentrador = new superficie*[n+1];
    concentrador[0] =  new sphere(vector3D(0,-1000,0), 1000, new matte(vector3D(0.5, 0.5, 0.5)));
    int i = 1;
    for (int a = -14; a < 14; a++) {
        for (int b = -14; b < 14; b++) {
            float choose_mat = float(rand())/RAND_MAX;
            vector3D center(a+0.9*float(rand())/RAND_MAX,0.2,b+0.9*float(rand())/RAND_MAX);
            if ((center-vector3D(4,0.2,0)).length() > 0.9) {
                if (choose_mat < 0.8) {
                    concentrador[i++] = new sphere(center, 0.2, new matte(vector3D(float(rand())/RAND_MAX* float(rand())/RAND_MAX, float(rand())/RAND_MAX* float(rand())/RAND_MAX, float(rand())/RAND_MAX* float(rand())/RAND_MAX)));
                }
                else if (choose_mat < 0.95) {
                    concentrador[i++] = new sphere(center, 0.2,
                            new metalico(vector3D(0.5*(1 + float(rand())/RAND_MAX), 0.5*(1 + float(rand())/RAND_MAX), 0.5*(1 + float(rand())/RAND_MAX)),  0.5*float(rand())/RAND_MAX));
                }
                else {
                    concentrador[i++] = new sphere(center, 0.2, new cristalino_dielectrico(1.5));
                }
            }
        }
    }

    concentrador[i++] = new sphere(vector3D(-4, 1, -6), 1.0, new matte(vector3D(0.4, 0.2, 0.1)));
    concentrador[i++] = new sphere(vector3D(-4, 1, -1), 1.0, new matte(vector3D(0.1, 0.9, 0.8)));
    concentrador[i++] = new sphere(vector3D(4, 1, 0), 1.0, new metalico(vector3D(0.7, 0.4, 0.5), 0.0));
    concentrador[i++] = new sphere(vector3D(8, 1, 4), 1.0, new metalico(vector3D(0.33, 0.9, 0.5), 0.0));

    return new ll_superficies(concentrador,i);
}

vector3D generar_color(const rayito_de_luz& r, superficie *universo, int profundidad) {
    registro_impactos registro;
    if (universo->hit(r, 0.001, FLT_MAX, registro)) {
        rayito_de_luz rayo_default;
        vector3D atenuador;
        if (profundidad < 50 && registro.mat_ptr->scatter(r, registro, atenuador, rayo_default)) {
             return atenuador*generar_color(rayo_default, universo, profundidad+1);
        }
        else {
            return vector3D(0,0,0);
        }
    }
    else {
        vector3D vector_unitario = unit_vector(r.direction());
        float t = 0.5*(vector_unitario.y() + 1.0);
        return (1.0-t)*vector3D(1.0, 1.0, 1.0) + t*vector3D(0.5, 0.7, 1.0);
    }
}
